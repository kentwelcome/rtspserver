//
//  VideoFrameProvider.hpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 16/01/2018.
//

#ifndef VideoFrameProvider_hpp
#define VideoFrameProvider_hpp

#ifndef ShareBuffer_hpp
#include "ShareBuffer.hpp"
#endif

#include <thread>

class VideoFrameProvider {
    std::thread* readerThread = NULL;
    
private:
    VideoFrameProvider(const char* path, int fps);
    ~VideoFrameProvider();
    
public:
    static VideoFrameProvider* getInstance(const char* path, int fps);
    void startWoker();
    void stopWoker();
    const char* path = NULL;
    int fps;
    ShareBuffer* frameBuffer;
    ShareRingBuffer* frameBufferQueue;
};
#endif /* VideoFrameProvider_hpp */
