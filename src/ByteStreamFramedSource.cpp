//
//  ByteStreamFramedSource.cpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 15/01/2018.
//

#include "ByteStreamFramedSource.hpp"

ByteStreamFramedSource* ByteStreamFramedSource::createNew(UsageEnvironment &env, ShareRingBuffer* frameBufferQueue) {
    ByteStreamFramedSource* newSource = new ByteStreamFramedSource(env, frameBufferQueue);
    return newSource;
}

ByteStreamFramedSource::ByteStreamFramedSource(UsageEnvironment &env, ShareRingBuffer* frameBufferQueue) : FramedSource(env), frameBufferQueue(frameBufferQueue){
    currentIndex = -1;
    timestamp = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());
}

ByteStreamFramedSource::~ByteStreamFramedSource() {
}

unsigned int ByteStreamFramedSource::maxFrameSize() const {
    return 100000;
}

void ByteStreamFramedSource::doGetNextFrame() {
    getNextFrame();
}

void ByteStreamFramedSource::getNextFrame0(void *clientData) {
    ((ByteStreamFramedSource*)clientData)->getNextFrame();
}

void ByteStreamFramedSource::getNextFrame() {
    size_t size = 0;
    int idx = 0;
    std::chrono::milliseconds currentTime = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());
    
    idx = frameBufferQueue->pop(fTo, &size);
    if (idx >= 0) {
        fFrameSize = size;
        if (fFrameSize > fMaxSize) {
            fNumTruncatedBytes = fFrameSize - fMaxSize;
            printf("TruncatedBytes: %d in frame[%d]\n", fNumTruncatedBytes, idx);
            fFrameSize = fMaxSize;
        }
        else {
            fNumTruncatedBytes = 0;
        }
        gettimeofday(&fPresentationTime, NULL);
        
        printf("getNextFrame[%d]-> FrameSize: %d MaxSize: %d duration: %lld\n", idx, fFrameSize, fMaxSize, currentTime - timestamp);
        timestamp = currentTime;
        if (size == 0) {
            nextTask() = envir().taskScheduler().scheduleDelayedTask(0, (TaskFunc*)FramedSource::handleClosure, this);
            return;
        }
    } else {
        nextTask() = envir().taskScheduler().scheduleDelayedTask(0, (TaskFunc*)ByteStreamFramedSource::getNextFrame0, this);
        return;
    }
    
    nextTask() = envir().taskScheduler().scheduleDelayedTask(0,(TaskFunc*)FramedSource::afterGetting, this);
}

void ByteStreamFramedSource::doStopGettingFrames() {
    printf("StopGettingFrame: %p\n", this);
    frameBufferQueue->terminate();
}

