//
//  ShareBuffer.hpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 16/01/2018.
//

#ifndef ShareBuffer_hpp
#define ShareBuffer_hpp

#include <mutex>

#pragma mark - ShareBuffer
class ShareBuffer {
public:
    ShareBuffer(size_t size);
    ~ShareBuffer();
    
    int push(char* const buffer, size_t size);
    int pop(char* const buffer, size_t* size);
    
    void setup();
    void terminate();
    
    int getBufferSize();
    int getIndex();
    
protected:
    int index;
    std::atomic<bool> isTerminate;
    size_t maxBufferSize_;
    size_t bufferSize_;
    std::mutex m_lock_;
    char* const buffer_;
    
};

#pragma mark - ShareRingBuffer
typedef struct ringBuffer {
    unsigned char* buffer;
    size_t size;
    bool isDirty;
} RingBuffer;

class ShareRingBuffer {
public:
    ShareRingBuffer(unsigned int numOfBuffer, size_t bufferSize);
    ~ShareRingBuffer();
    
    int push(unsigned char* inBuffer, size_t size);
    int pop(unsigned char* outBuffer, size_t* size);
    
    void reset();
    void terminate();
    
protected:
    int index;
    int numOfBuffer;
    size_t bufferSize;
    unsigned int writeIdx;
    unsigned int readIdx;
    RingBuffer* ringBuffer;
    std::atomic<bool> isTerminate;
    std::mutex m_lock_;
};

#endif /* ShareBuffer_hpp */
