//
//  RtspServer.cpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 04/01/2018.
//

#include "RtspServer.hpp"
#include <liveMedia.hh>
#include "H264VideoStreamServerMediaSubsession.hpp"

static ServerMediaSession* createNewMemorySMS(UsageEnvironment& env,
                                        char const* streamName, int fps);
static ServerMediaSession* createNewFileSMS(UsageEnvironment& env,
                                            char const* streamName,
                                            char const* filename);

UmboRtspServer* UmboRtspServer::createNew(UsageEnvironment& env,
                                          Port port,
                                          UserAuthenticationDatabase* authDatabase,
                                          unsigned reclamationTestSeconds) {
    int ourSocket = setUpOurSocket(env, port);
    if (ourSocket == -1) { return NULL; }
    
    return new UmboRtspServer(env, ourSocket, port, authDatabase, reclamationTestSeconds);
}

UmboRtspServer::UmboRtspServer(UsageEnvironment& env,
                               int ourSocket,
                               Port port,
                               UserAuthenticationDatabase* authDatabase,
                               unsigned reclamationTestSeconds)
: RTSPServerSupportingHTTPStreaming(env, ourSocket, port, authDatabase, reclamationTestSeconds) {
    streamSource = MEMORY_SOURCE;
    streamName = STREAM_NAME;
    fps = 10;
}

UmboRtspServer::~UmboRtspServer(){
}

void UmboRtspServer::setStreamSrouce(const char *source) {
    streamSource = std::string(source);
}

void UmboRtspServer::setStreamName(const char *name) {
    streamName = std::string(name);
}

void UmboRtspServer::setStreamFilename(const char *filename) {
    streamFilename = std::string(filename);
}

void UmboRtspServer::setStreamFps(int fps) {
    this->fps = fps;
}

ServerMediaSession* UmboRtspServer::lookupServerMediaSession(const char *name, Boolean isFirstLookupInSession){
    ServerMediaSession* sms = RTSPServer::lookupServerMediaSession(name);
    Boolean smsExists = sms != NULL;
    
    printf("New connection -> %s %d\n", name, isFirstLookupInSession);
    if (std::string(name) != streamName) {
        return NULL;
    }
    
    if (smsExists && isFirstLookupInSession) {
        removeServerMediaSession(sms);
        sms = NULL;
    }
    
    if (sms == NULL) {
        if (streamSource == std::string(MEMORY_SOURCE)) {
            sms = createNewMemorySMS(envir(), streamName.c_str(), fps);
        }
        else if (streamSource == std::string(FILE_SOURCE)) {
            sms = createNewFileSMS(envir(), streamName.c_str(), streamFilename.c_str());
        }
    }
    
    if (sms != NULL) {
        addServerMediaSession(sms);
    }
    return sms;
}

static ServerMediaSession* createNewFileSMS(UsageEnvironment& env,
                                            char const* streamName,
                                            char const* filename) {
    ServerMediaSession* sms = NULL;
    Boolean const reuseSource = False;
    
    FILE* fd = fopen(filename, "rb");
    if (fd == NULL) {
        env << "File '" << filename << "' not found.\n";
        return NULL;
    }
    fclose(fd);
    sms = ServerMediaSession::createNew(env, streamName, streamName, "H.264 Video");
    if (sms) {
        OutPacketBuffer::maxSize = 100000; // allow for some possibly large H.264 frames
        sms->addSubsession(H264VideoFileServerMediaSubsession::createNew(env, filename, reuseSource));
    }
    
    return sms;
}

static ServerMediaSession* createNewMemorySMS(UsageEnvironment& env,
                                        char const* streamName, int fps) {
    ServerMediaSession* sms = NULL;
    Boolean const reuseSource = False;
    sms = ServerMediaSession::createNew(env, streamName, streamName, "H.264 Video");
    if (sms) {
        OutPacketBuffer::maxSize = 1000000; // allow for some possibly large H.264 frames
        sms->addSubsession(H264VideoStreamServerMidiaSubsession::createNew(env, streamName, fps, reuseSource));
    }
    //TODO: ...
    return sms;
}
