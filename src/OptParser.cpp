//
//  OptParser.cpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 05/01/2018.
//

#include "OptParser.hpp"
#include "asciicolor.h"
#include "version.h"
using namespace  std;

#pragma mark - Class OptParser
static OptParser* instance_ = NULL;

#pragma mark - Private Method
OptParser::OptParser(int argc, const char ** argv)
{
    opt_parser_ = new cmdline::parser();
    argc_ = argc;
    argv_ = argv;
    
    declareOptions();
}

void OptParser::declareOptions()
{
    opt_parser_->add<string>(OPTION_STREAM_NAME, '\0',
                             "RTSP Stream name",
                             false,
                             "stream");
    opt_parser_->add<string>(OPTION_STREAM_SOURCE, '\0',
                            "RTSP Stream source <file || memory>",
                             false,
                             OPT_TYPE_MEMORY,
                             cmdline::oneof<string>(OPT_TYPE_FILE, OPT_TYPE_MEMORY));
    opt_parser_->add<string>(OPTION_STREAM_FILENAME, '\0',
                             "RTSP Stream source file path",
                             false);
    
    opt_parser_->add<int>(OPTION_RTSP_PORT, 'p',
                          "RTSP server port",
                          false,
                          8554,
                          cmdline::range(1, 65535));
    
    opt_parser_->add<int>(OPTION_RTSP_FPS, '\0',
                          "RTSP streaming fps",
                          false,
                          10,
                          cmdline::range(10, 30));
    
    opt_parser_->add<string>(OPTION_AUTH_USERNAME, '\0',
                             "Authentication username",
                             false);
    opt_parser_->add<string>(OPTION_AUTH_PASSWORD, '\0',
                             "Authentication password",
                             false);
    
    opt_parser_->add(OPTION_VERSION, 'v', "Print the version infomation");
    opt_parser_->add(OPTION_HELP, 'h', "Print this message");
}

#pragma mark - Public Method
OptParser::~OptParser()
{
    instance_ = NULL;
    delete opt_parser_;
}

OptParser* OptParser::GetInstance(int argc, const char ** argv){
    if (argc == 0 || argv == NULL) {
        return NULL;
    }
    if (!instance_) {
        instance_ = new OptParser(argc, argv);
    }
    return instance_;
}

int OptParser::Parse()
{
    if (opt_parser_->parse(argc_, argv_)) {
        return 0;
    }
    else {
        return -1;
    }
}

void OptParser::ShowSyntaxError()
{
    string err = opt_parser_->error();
    cerr << err << endl;
}

void OptParser::ShowUsage()
{
    string usage = opt_parser_->usage();
    cerr << usage << endl;
}

void OptParser::ShowVersionInfo()
{
    cerr << RTSPSTREAMPROVIDER_NAME_VERSION << endl;
}

void OptParser::ParseAndCheck()
{
    opt_parser_->parse_check(argc_, (char **)argv_);
}

const char* OptParser::GetString(const char *key)
{
    const char* val = NULL;
    if (key) {
        val = opt_parser_->get<string>(string(key)).c_str();
        
        if (val[0] == '\0'){
            val = NULL;
        }
    }
    return val;
}

int OptParser::GetNumber(const char *key)
{
    int val = 0;
    if (key) {
        val = opt_parser_->get<int>(string(key));
    }
    return val;
}

unsigned int OptParser::GetUnsignedInt(const char *key)
{
    unsigned int val = 0;
    if (key) {
        val = (unsigned int) opt_parser_->get<int>(string(key));
    }
    return val;
}

bool OptParser::GetBool(const char *key)
{
    bool val = false;
    if (key) {
        val = opt_parser_->exist(string(key));
    }
    return val;
}

uint64_t OptParser::GetHex64(const char *key)
{
    uint64_t hex64 = 0;
    const char* hex_str = GetString(key);
    char *pEnd = NULL;
    
    if (hex_str) {
        hex64 = strtoull(hex_str, &pEnd, 16);
    }
    
    return hex64;
}

int OptParser::GetHex(const char *key)
{
    int hex = 0;
    const char *hex_str = GetString(key);
    char *pEnd = NULL;
    
    if (hex_str) {
        hex = strtol(hex_str, &pEnd, 16);
    }
    
    return hex;
}

cmdline::parser* OptParser::GetOptions()
{
    return opt_parser_;
}

