//
//  ByteStreamFramedSource.hpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 15/01/2018.
//

#ifndef ByteStreamFramedSource_hpp
#define ByteStreamFramedSource_hpp

#ifndef _FRAMED_SOURCE_HH
#include "FramedSource.hh"
#endif

#ifndef ShareBuffer_hpp
#include "ShareBuffer.hpp"
#endif


class ByteStreamFramedSource: public FramedSource {
public:
    static ByteStreamFramedSource* createNew(UsageEnvironment& env, ShareRingBuffer* frameBufferQueue);
protected:
    ByteStreamFramedSource(UsageEnvironment& env, ShareRingBuffer* frameBufferQueue); // abstract base class
    virtual ~ByteStreamFramedSource();
    ShareRingBuffer* frameBufferQueue;

private:
    virtual unsigned int maxFrameSize() const;
    virtual void doGetNextFrame();
    static void getNextFrame0(void* clientData);
    void getNextFrame();
    int currentIndex;
    virtual void doStopGettingFrames();
    std::chrono::milliseconds timestamp;
};

#endif /* ByteStreamFramedSource_hpp */
