//
//  H264VideoStreamServerMediaSubsession.hpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 08/01/2018.
//

#ifndef H264VideoStreamServerMediaSubsession_hpp
#define H264VideoStreamServerMediaSubsession_hpp

#ifndef _SERVER_MEDIA_SESSION_HH
#include "ServerMediaSession.hh"
#endif
#ifndef _ON_DEMAND_SERVER_MEDIA_SUBSESSION_HH
#include "OnDemandServerMediaSubsession.hh"
#endif

class H264VideoStreamServerMidiaSubsession: public OnDemandServerMediaSubsession {
    public:
    static H264VideoStreamServerMidiaSubsession* createNew(UsageEnvironment& env, char const* streamName, int fps, Boolean reuseFirstSource);
    
    // Used to implement "getAuxSDPLine()":
    void checkForAuxSDPLine1();
    void afterPlayingDummy1();

protected:
    H264VideoStreamServerMidiaSubsession(UsageEnvironment& env, char const* streamName, int fps, Boolean reuseFirstSource);
    // called only by createNew();
    virtual ~H264VideoStreamServerMidiaSubsession();
    
    void setDoneFlag() { fDoneFlag = ~0; }
    
protected: // redefined virtual functions
    virtual char const* getAuxSDPLine(RTPSink* rtpSink,
                                      FramedSource* inputSource);
    virtual FramedSource* createNewStreamSource(unsigned clientSessionId,
                                                unsigned& estBitrate);
    virtual RTPSink* createNewRTPSink(Groupsock* rtpGroupsock,
                                      unsigned char rtpPayloadTypeIfDynamic,
                                      FramedSource* inputSource);
    
private:
    int fps;
    char* fAuxSDPLine;
    char fDoneFlag; // used when setting up "fAuxSDPLine"
    RTPSink* fDummyRTPSink; // ditto
};

#endif /* H264VideoStreamServerMediaSubsession_hpp */
