//
//  ShareBuffer.cpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 16/01/2018.
//

#include "ShareBuffer.hpp"

#pragma mark - ShareBuffer
ShareBuffer::ShareBuffer(size_t size) : index(-1), isTerminate(false),  maxBufferSize_(size), bufferSize_(0),   buffer_((char* const)malloc(size)) {
}

ShareBuffer::~ShareBuffer() {
    if (buffer_) {
        free(buffer_);
    }
}

int ShareBuffer::push(char *const buffer, size_t size) {
    if (isTerminate == true) {return -1;}
    
    if (buffer == NULL || size > maxBufferSize_) { return -1; }
    m_lock_.lock();
    memcpy(buffer_, buffer, size);
    bufferSize_ = size;
    index++;
    m_lock_.unlock();
    return 0;
}

int ShareBuffer::pop(char *const buffer, size_t *size) {
    if (isTerminate == true) {return -1;}
    
    int idx = 0;
    if (buffer == NULL || size == NULL) { return -1; }
    m_lock_.lock();
    memcpy(buffer, buffer_, bufferSize_);
    *size = bufferSize_;
    idx = index;
    m_lock_.unlock();
    return idx;
}

int ShareBuffer::getBufferSize() {
    int size = 0;
    m_lock_.lock();
    size = bufferSize_;
    m_lock_.unlock();
    return size;
}

int ShareBuffer::getIndex() {
    int idx = 0;
    m_lock_.lock();
    idx = index;
    m_lock_.unlock();
    return idx;
}

void ShareBuffer::setup() {
    isTerminate = false;
    index = -1;
}

void ShareBuffer::terminate() {
    isTerminate = true;
}

#pragma mark - ShareRingBuffer
ShareRingBuffer::ShareRingBuffer(unsigned int numOfBuffer, size_t bufferSize) : index(-1), numOfBuffer(numOfBuffer), bufferSize(bufferSize), writeIdx(0), readIdx(0), isTerminate(false) {
    int i = 0;
    this->ringBuffer = (RingBuffer*) malloc(sizeof(RingBuffer) * numOfBuffer);
    if (this->ringBuffer == NULL) {
        throw std::bad_alloc();
    }
    for (i = 0; i < numOfBuffer; i++) {
        this->ringBuffer[i].buffer = (unsigned char*) malloc(sizeof(unsigned char) * bufferSize);
        this->ringBuffer[i].size = 0;
        this->ringBuffer[i].isDirty = false;
        if (this->ringBuffer[i].buffer == NULL) {
                throw std::bad_alloc();
        }
    }
}

ShareRingBuffer::~ShareRingBuffer() {
    int i = 0;
    if (this->ringBuffer) {
        for (i = 0; i < numOfBuffer; i++) {
            free(this->ringBuffer[i].buffer);
        }
        free(this->ringBuffer);
        this->ringBuffer = NULL;
    }
}

int ShareRingBuffer::push(unsigned char *inBuffer, size_t size) {
    int idx = 0;
    
    if (isTerminate || inBuffer == NULL || size > bufferSize) { return -1; }

    m_lock_.lock();
    memcpy(this->ringBuffer[writeIdx%numOfBuffer].buffer, inBuffer, size);
    this->ringBuffer[writeIdx%numOfBuffer].size = size;
    this->ringBuffer[writeIdx%numOfBuffer].isDirty = true;
    idx = writeIdx++;
    m_lock_.unlock();

    return idx;
}

int ShareRingBuffer::pop(unsigned char *outBuffer, size_t *size) {
    int idx = 0;
    int queueSize = 0;
    if (isTerminate || outBuffer == NULL || size == NULL) { return -1; }
    
    m_lock_.lock();
    if (this->ringBuffer[readIdx%numOfBuffer].isDirty) {
        memcpy(outBuffer, this->ringBuffer[readIdx%numOfBuffer].buffer, this->ringBuffer[readIdx%numOfBuffer].size);
        *size = this->ringBuffer[readIdx%numOfBuffer].size;
        this->ringBuffer[readIdx%numOfBuffer].isDirty = false;
        idx = readIdx++;
        queueSize = writeIdx - readIdx;
    } else {
        idx = -1;
    }
    m_lock_.unlock();
    if (queueSize > numOfBuffer/2) {
        printf("Queue: %d\n", queueSize);
    }
    return idx;
}

void ShareRingBuffer::reset() {
    int i = 0;
    m_lock_.lock();
    index = -1;
    writeIdx = 0;
    readIdx = 0;
    isTerminate = false;
    
    for (i = 0; i < numOfBuffer; i++) {
        this->ringBuffer[i].size = 0;
        this->ringBuffer[i].isDirty = false;
    }
    m_lock_.unlock();
}

void ShareRingBuffer::terminate() {
    isTerminate = true;
}
