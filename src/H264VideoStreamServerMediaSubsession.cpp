//
//  H264VideoStreamServerMediaSubsession.cpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 08/01/2018.
//

#include "H264VideoStreamServerMediaSubsession.hpp"
#include "H264VideoRTPSink.hh"
#include "H264VideoStreamFramer.hh"
#include "ByteStreamFramedSource.hpp"
#include "VideoFrameProvider.hpp"
#include "ShareBuffer.hpp"

H264VideoStreamServerMidiaSubsession* H264VideoStreamServerMidiaSubsession::createNew(UsageEnvironment &env, const char *streamName, int fps, Boolean reuseFirstSource) {
    return new H264VideoStreamServerMidiaSubsession(env, streamName, fps, reuseFirstSource);
}

H264VideoStreamServerMidiaSubsession::H264VideoStreamServerMidiaSubsession(UsageEnvironment &env, const char *streamName, int fps, Boolean reuseFirstSource) : OnDemandServerMediaSubsession(env, reuseFirstSource), fps(fps) {
    fDoneFlag = 0;
    fAuxSDPLine = NULL;
    fDummyRTPSink = NULL;
}

H264VideoStreamServerMidiaSubsession::~H264VideoStreamServerMidiaSubsession() {
    delete [] fAuxSDPLine;
}

static void afterPlayingDummy(void* clientData) {
    H264VideoStreamServerMidiaSubsession* subsess = (H264VideoStreamServerMidiaSubsession*)clientData;
    subsess->afterPlayingDummy1();
}

static void checkForAuxSDPLine(void* clientData) {
    H264VideoStreamServerMidiaSubsession* subsess = (H264VideoStreamServerMidiaSubsession*)clientData;
    subsess->checkForAuxSDPLine1();
}

void H264VideoStreamServerMidiaSubsession::checkForAuxSDPLine1() {
    nextTask() = NULL;
    char const* dasl = NULL;
    
    if (fAuxSDPLine != NULL) {
        setDoneFlag();
    } else if (fDummyRTPSink != NULL && (dasl = fDummyRTPSink->auxSDPLine()) != NULL) {
        fAuxSDPLine = strDup(dasl);
        fDummyRTPSink = NULL;
        
        setDoneFlag();
    } else if (!fDoneFlag) {
        // try again after a brief delay:
        int uSecsToDelay = 100000; // 100 ms
        nextTask() = envir().taskScheduler().scheduleDelayedTask(uSecsToDelay, (TaskFunc*)checkForAuxSDPLine, this);
    }
}

void H264VideoStreamServerMidiaSubsession::afterPlayingDummy1() {
    envir().taskScheduler().unscheduleDelayedTask(nextTask());
    setDoneFlag();
}

char const* H264VideoStreamServerMidiaSubsession::getAuxSDPLine(RTPSink *rtpSink, FramedSource *inputSource) {
    if (fAuxSDPLine != NULL) return fAuxSDPLine; // it's already been set up (for a previous client)
    
    if (fDummyRTPSink == NULL) { // we're not already setting it up for another, concurrent stream
        // Note: For H264 video files, the 'config' information ("profile-level-id" and "sprop-parameter-sets") isn't known
        // until we start reading the file.  This means that "rtpSink"s "auxSDPLine()" will be NULL initially,
        // and we need to start reading data from our file until this changes.
        fDummyRTPSink = rtpSink;
        
        // Start reading the file:
        fDummyRTPSink->startPlaying(*inputSource, afterPlayingDummy, this);
        
        // Check whether the sink's 'auxSDPLine()' is ready:
        checkForAuxSDPLine(this);
    }
    
    envir().taskScheduler().doEventLoop(&fDoneFlag);
    
    return fAuxSDPLine;
}

FramedSource* H264VideoStreamServerMidiaSubsession::createNewStreamSource(unsigned int clientSessionId, unsigned int &estBitrate) {
    estBitrate = 500; // kbps, estimate
    
    // Create the video source:
    VideoFrameProvider* frameProvider = VideoFrameProvider::getInstance("./test_frames/", fps);
    FramedSource* newFramedSource = ByteStreamFramedSource::createNew(envir(), frameProvider->frameBufferQueue);
    if (newFramedSource == NULL) {
        return NULL;
    }
    frameProvider->stopWoker();
//    usleep(500000);
    frameProvider->startWoker();
    // Create a framer for the Video Elementary Stream:
    return H264VideoStreamFramer::createNew(envir(), newFramedSource);
}

RTPSink* H264VideoStreamServerMidiaSubsession::createNewRTPSink(Groupsock *rtpGroupsock, unsigned char rtpPayloadTypeIfDynamic, FramedSource *inputSource) {
    return H264VideoRTPSink::createNew(envir(), rtpGroupsock, rtpPayloadTypeIfDynamic);
}
