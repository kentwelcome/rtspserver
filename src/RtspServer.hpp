//
//  RtspServer.hpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 04/01/2018.
//

#ifndef RtspServer_hpp
#define RtspServer_hpp

#ifndef _RTSP_SERVER_SUPPORTING_HTTP_STREAMING_HH
#include "RTSPServerSupportingHTTPStreaming.hh"
#endif
#include <string>

#define STREAM_NAME "stream"

#define MEMORY_SOURCE "memory"
#define FILE_SOURCE "file"

class UmboRtspServer: public RTSPServerSupportingHTTPStreaming {
public:
    void setStreamSrouce(const char* source);
    void setStreamName(const char* name);
    void setStreamFilename(const char* filename);
    void setStreamFps(int fps);
    static UmboRtspServer* createNew(UsageEnvironment& env,
                                     Port port,
                                     UserAuthenticationDatabase* authDatabase,
                                     unsigned reclamationTestSeconds = 65);
    
    
protected:
    UmboRtspServer(UsageEnvironment& env,
                   int ourSocket,
                   Port port,
                   UserAuthenticationDatabase* authDatabase,
                   unsigned reclamationTestSeconds);
    // called only by createNew();
    virtual ~UmboRtspServer();
    
    std::string streamSource;
    std::string streamName;
    std::string streamFilename;
    int fps;
    
protected:
    virtual ServerMediaSession* lookupServerMediaSession(char const* streamName, Boolean isFirstLookupInSession);
};
#endif /* RtspServer_hpp */
