#include <stdio.h>
#include "version.h"

#include <BasicUsageEnvironment.hh>
#include "OptParser.hpp"
#include "RtspServer.hpp"
#include "VideoFrameProvider.hpp"

OptParser* parseCommandLineOptions(int argc, const char **argv) {
    int rc = 0;
    OptParser* opts = OptParser::GetInstance(argc, argv);
    
    rc = opts->Parse();
    if (opts->GetBool("help")) {
        opts->ShowUsage();
        return NULL;
    }
    else if (rc != 0) {
        opts->ShowSyntaxError();
        opts->ShowUsage();
        return NULL;
    }
    
    if (opts->GetBool(OPTION_VERSION) == true) {
        opts->ShowVersionInfo();
        return NULL;
    }
    return opts;
}

int main(int argc, const char **argv) {
    OptParser* opts = parseCommandLineOptions(argc, argv);
    if (opts == NULL) {
        exit(1);
    }
    // Begin by setting up our usage environment:
    TaskScheduler* scheduler = BasicTaskScheduler::createNew();
    UsageEnvironment* env = BasicUsageEnvironment::createNew(*scheduler);
    UserAuthenticationDatabase* authDB = NULL;
    UmboRtspServer* rtspServer = NULL;
    portNumBits rtspServerPort = opts->GetNumber(OPTION_RTSP_PORT);
    
    if (opts->GetString(OPTION_AUTH_USERNAME) != NULL && opts->GetString(OPTION_AUTH_PASSWORD) != NULL) {
        authDB = new UserAuthenticationDatabase;
        authDB->addUserRecord("username1", "password1"); // replace these with real strings
    }
    
    
    
    rtspServer = UmboRtspServer::createNew(*env, rtspServerPort, authDB);
    if (rtspServer == NULL) {
        *env << "Fail to create RTSP server: " << env->getResultMsg() << "\n";
        exit(1);
    }
    rtspServer->setStreamSrouce(opts->GetString(OPTION_STREAM_SOURCE));
    rtspServer->setStreamName(opts->GetString(OPTION_STREAM_NAME));
    rtspServer->setStreamFps(opts->GetNumber(OPTION_RTSP_FPS));
    if (strcmp(opts->GetString(OPTION_STREAM_SOURCE), OPT_TYPE_FILE) == 0) {
        const char *filename = opts->GetString(OPTION_STREAM_FILENAME);
        if (filename == NULL) {
            std::cerr << "Should provide filename when source is 'file'." << std::endl;
            exit(1);
        }
        rtspServer->setStreamFilename(filename);
    }
    
    *env << "Umbo RTSP Stream Provider\n";
    *env << "\tversion: " << RTSPSTREAMPROVIDER_NAME_VERSION << "\n";
    
    char *urlPrefix = rtspServer->rtspURLPrefix();
    *env << "Play streams from this server using the URL\n\t"
    << urlPrefix << opts->GetString(OPTION_STREAM_NAME) << "\n";
    
    env->taskScheduler().doEventLoop(); // does not return
    return 0;
}
