//
//  OptParser.hpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 05/01/2018.
//

#ifndef OptParser_hpp
#define OptParser_hpp

#include "cmdline.h"

#define OPT_TYPE_FILE               "file"
#define OPT_TYPE_MEMORY             "memory"

#define OPTION_STREAM_SOURCE        "source"
#define OPTION_STREAM_FILENAME      "filename"
#define OPTION_STREAM_NAME          "name"

#define OPTION_RTSP_PORT            "port"
#define OPTION_RTSP_FPS             "fps"

#define OPTION_AUTH_USERNAME        "username"
#define OPTION_AUTH_PASSWORD        "password"

#define OPTION_VERSION              "version"
#define OPTION_HELP                 "help"


#define COLOR_OPT(key,val) FMAG(key) << "=" << FGRN(val) << " "

class OptParser {
private:
    
    int                 argc_;
    const char**        argv_;
    cmdline::parser     *opt_parser_;
    
    // Method
    OptParser(int argc, const char * argv[]);
    void autoImportOptions();
    void declareOptions();
    
public:
    static OptParser* GetInstance(int argc, const char ** argv);
    ~OptParser();
    // Variable
    
    // Method
    int Parse();
    void ShowSyntaxError();
    void ShowUsage();
    void ShowVersionInfo();
    void ParseAndCheck();
    cmdline::parser* GetOptions();
    
    const char*     GetString(const char* key);
    int             GetNumber(const char* key);
    unsigned int    GetUnsignedInt(const char* key);
    bool            GetBool(const char* key);
    uint64_t        GetHex64(const char* key);
    int             GetHex(const char* key);
    
};

#endif /* OptParser_hpp */
