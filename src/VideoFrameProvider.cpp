//
//  VideoFrameProvider.cpp
//  RtspStreamProvider
//
//  Created by Kent Huang on 16/01/2018.
//

#include <iostream>
#include "VideoFrameProvider.hpp"
#include <unistd.h>

static VideoFrameProvider* instance = NULL;

VideoFrameProvider* VideoFrameProvider::getInstance(const char* path, int fps) {
    if (instance == NULL) {
        instance = new VideoFrameProvider(path, fps);
    }
    return instance;
}

VideoFrameProvider::VideoFrameProvider(const char* path, int fps) {
    this->path = path;
    this->fps = fps;
    this->frameBufferQueue = new ShareRingBuffer(100, 150000);
}

VideoFrameProvider::~VideoFrameProvider() {
    if (readerThread != NULL) {
        delete readerThread;
    }
}

void frameReaderWorkerThreadRuntine(const char* path, ShareRingBuffer* frameBufferQueue, int fps) {
    FILE *fd = NULL;
    char filename[32] = {0};
    unsigned char *mem;
    long size = 0;
    long filesize = 0;
    double delay = 1000.0 / (fps);  // ms
    int to_delay = delay * 1000;  // us
    int rc;
    std::thread::id tid =std::this_thread::get_id();
//    printf("Start worker thread %p with buffer: %p\n", this_id, frameBufferQueue);
    std::cout  << "Start worker thread " << tid << std::endl;
    chdir(path);
    mem = (unsigned char*)malloc(150000);
    frameBufferQueue->reset();
    printf("Start playing video\n");
    for (int i = 0; i < 1000; i++) {
        size = sprintf(filename, "stream0-%d", i);
        filename[size] = '\0';
        fd = fopen(filename, "rb");
        if (fd == NULL) {
            continue;
        }
        
        fseek (fd, 0, SEEK_END);   // non-portable
        filesize = ftell(fd);
        fseek (fd, 0, SEEK_SET);
        fread(mem, 1, filesize, fd);
        fclose(fd);
        
        rc = frameBufferQueue->push(mem, filesize);
        //        printf("Frame[%d] size: %ld\n", i, filesize);
        if (rc < 0 ) { break; }
        usleep(to_delay);
    }
    std::cout  << "Stop worker thread " << tid << std::endl;
    rc = frameBufferQueue->push(mem, 0);
    free(mem);
    return;
}

void VideoFrameProvider::startWoker() {
    if (readerThread == NULL) {
        readerThread = new std::thread(frameReaderWorkerThreadRuntine, path, frameBufferQueue, fps);
//        readerThread->detach();
    }
}

void VideoFrameProvider::stopWoker() {
    if (readerThread) {
        readerThread->join();
        readerThread->~thread();
        readerThread = NULL;
    }
}
