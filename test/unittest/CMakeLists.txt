
# 
# Executable name and options
# 

# Target name
set(target unittest)
message(STATUS "Test ${target}")


# 
# Sources
# 

set(sources
    ${UNITTEST_SOURCES}
)

#
# Tested Sources and Headers
#
set(testedSources
    ${UNITTEST_TARGET_SOURCES}
)

set(testedHeaders
    ${UNITTEST_TARGET_HEADERS}
)

# 
# Create executable
# 

# Build executable
add_executable(${target}
    ${sources}
    ${testedSources}
    ${testedHeaders}
)

# Create namespaced alias
add_executable(${META_PROJECT_NAME}::${target} ALIAS ${target})


# 
# Project options
# 

set_target_properties(${target}
    PROPERTIES
    ${DEFAULT_PROJECT_OPTIONS}
    FOLDER "${IDE_FOLDER}"
)


# 
# Include directories
# 

target_include_directories(${target}
    PRIVATE
    ${DEFAULT_INCLUDE_DIRECTORIES}
    ${PROJECT_BINARY_DIR}/include
    ${PROJECT_SOURCE_DIR}/include
    ${PROJECT_SOURCE_DIR}/src
    ${IMPORT_INCLUDE_DIRECTORIES}
)

# 
# Libraries
# 

target_link_libraries(${target}
    PRIVATE
    ${DEFAULT_LIBRARIES}
    ${IMPORT_LIBRARIES}
    gtest
)


# 
# Compile definitions
# 

target_compile_definitions(${target}
    PRIVATE
    ${DEFAULT_COMPILE_DEFINITIONS}
)


# 
# Compile options
# 

target_compile_options(${target}
    PRIVATE
    ${DEFAULT_COMPILE_OPTIONS}
    ${IMPORT_COMPILER_OPTIONS}
)


# 
# Linker options
# 

target_link_libraries(${target}
    PRIVATE
    ${DEFAULT_LINKER_OPTIONS}
)
