# CMake version
cmake_minimum_required(VERSION 3.0 FATAL_ERROR)

find_package(PkgConfig REQUIRED)
pkg_search_module(GST REQUIRED gstreamer-1.0)

message("Module gstreamer-1.0 Found")

# Find GST Libraries with full path
set(GST_LINK_LIBRARIES)
foreach(_lib_name
    ${GST_LIBRARIES})
    set(FOUND_LIB "FOUND_LIB-NOTFOUND")
    find_library(FOUND_LIB
        NAMES ${_lib_name}
        PATHS ${GST_LIBRARY_DIRS})
    list(APPEND GST_LINK_LIBRARIES ${FOUND_LIB})
endforeach()

